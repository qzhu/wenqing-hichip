#!/usr/bin/python

import sys
import os

f = open(sys.argv[1])
option=sys.argv[2] #E10.5 or E13.5
dest=""
if option=="E10.5":
	dest = "orig.E10.5/10000"
else:
	dest = "orig.E13.5/10000"
i = 1
resolution=10000
for l in f:
	l = l.rstrip("\n")
	ll = l.split("\t")
	x = int(ll[1])
	y = int(ll[2])
	chr_id = ll[0].split("chr")[1]
	c_range = chr_id+":"+str(x)+":"+str(y)
	print c_range
	fname = "%d.%s-%d-%d" % (i, chr_id, x, y)
	if option=="E10.5":
		cmd = "java -jar ../juicer_tools.1.7.6_jcuda.0.8.jar dump observed VC_SQRT ../20181114_E10_5_Gata1_1029_WC6171_S1_001_mm10.bwt2pairs.validPairs.sorted.hic %s %s BP %d > %s/%s" % (c_range, c_range, resolution, dest, fname)
	else:
		cmd = "java -jar ../juicer_tools.1.7.6_jcuda.0.8.jar dump observed VC_SQRT ../20181114_E13_5_Gata1_1029_WC6171_S2_001_mm10.bwt2pairs.validPairs.sorted.hic %s %s BP %d > %s/%s" % (c_range, c_range, resolution, dest, fname)
	os.system(cmd)
	i+=1 
f.close()
