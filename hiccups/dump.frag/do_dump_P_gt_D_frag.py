#!/usr/bin/python

import sys
import os

f = open(sys.argv[1])
dest = "eryP_eryD_common_genes_P_gt_D_50k_interact_observed_none_frag/E105"
#dest = "eryP_eryD_common_genes_P_gt_D_50k_interact_observed_none_frag/E135"
i = 1
for l in f:
	l = l.rstrip("\n")
	ll = l.split("\t")
	coord = ll[-1].split(";")
	chr_id = ""
	ranges = []
	for ic in coord:
		ii = ic.split("_")
		chr_id = ii[1].split("chr")[1]
		ranges.append(int(ii[2]))
	#coord = ll[0].split("chr")[1]
	#x,y = ll[1], ll[2]
	x = min(ranges)
	y = max(ranges)
	mid = int((x + y)/2.0)
	c_range = chr_id+":"+str(mid-100)+":"+str(mid+100)
	print chr_id+":"+str(mid-100)+":"+str(mid+100)
	gene = ll[-2].split()[1]
	cmd = "java -jar juicer_tools.1.7.6_jcuda.0.8.jar dump observed NONE ../20181114_E10_5_Gata1_1029_WC6171_S1_001_mm10.bwt2pairs.validPairs.sorted.hic %s %s FRAG 1 > %s/%s.%s" % (c_range, c_range, dest, i, gene)
	#cmd = "java -jar juicer_tools.1.7.6_jcuda.0.8.jar dump observed NONE ../20181114_E13_5_Gata1_1029_WC6171_S2_001_mm10.bwt2pairs.validPairs.sorted.hic %s %s FRAG 1 > %s/%s.%s" % (c_range, c_range, dest, i, gene)
	#print cmd
	os.system(cmd)
	i+=1 
f.close()
