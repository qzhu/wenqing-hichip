#!/bin/bash
prog=juicer_tools.1.7.6_jcuda.0.8.jar
hicfile=20181114_E10_5_Gata1_1029_WC6171_S1_001_mm10.bwt2pairs.validPairs.sorted.hic
#hicfile=20181114_E13_5_Gata1_1029_WC6171_S2_001_mm10.bwt2pairs.validPairs.sorted.hic
bedfile=$1
outdir=$2
#prefix=E13_5
prefix=E10_5

mkdir $outdir

for i in `cat $bedfile`; do
java -jar $prog dump oe KR $hicfile $i $i BP 5000 $outdir/$prefix.$i.txt
done
