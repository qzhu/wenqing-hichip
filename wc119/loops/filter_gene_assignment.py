#!/usr/bin/python

import sys
for l in sys.stdin:
	l = l.rstrip("\n")
	ll = l.split("\t")
	genes = ll[-1].split(";")
	genes = sorted(set(genes))
	sys.stdout.write("%s\t%s\n" % ("\t".join(ll[:-1]), ";".join(genes)))
