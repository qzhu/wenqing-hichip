#!/usr/bin/python

import sys
import re
import os

def read_promoter_anchors(n):
	f = open(n)
	all_g = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		all_g[(ll[0],ll[1],ll[2])] = ll[3]
	f.close()
	return all_g

def read_enhancer_anchors(n):
	f = open(n)
	all_enhancers = set([])
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		all_enhancers.add((ll[0],ll[1],ll[2]))
	f.close()
	return all_enhancers

def read_gene_list(n):
	f = open(n)
	f.readline()
	genes = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		genes.append(ll[1])
	f.close()
	return genes

def read_loops(n):
	f = open(n)
	f.readline()
	loops = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		k1 = ("chr"+ll[0],ll[1],ll[2])
		k2 = ("chr"+ll[3],ll[4],ll[5])
		loops.setdefault(k1, set([]))
		loops[k1].add(k2)
		loops.setdefault(k2, set([]))
		loops[k2].add(k1)
	f.close()
	return loops
		
promoters = read_promoter_anchors("/tmp/promoter.overlapped.bed")
enhancers = read_enhancer_anchors("/tmp/enhancer.overlapped.bed")
loops = read_loops("/tmp/all.loops.bedpe")
genes = read_gene_list(sys.argv[1])

tot=0
num_g = 0
enh = 0
non_enh = 0
pr = 0
self_pr = 0
for p in promoters:
	if promoters[p] in set(genes):
		print promoters[p], "is present", len(loops[p])
		num_g+=1
		for pi in loops[p]:
			if pi in enhancers:
				#print "Is an enhancer loop"
				enh+=1
			elif pi in promoters:
				pr+=1
				if pi==p:
					self_pr+=1
			else:
				#print "Is NOT an enhancer loop"
				non_enh+=1
			tot+=1

print "Total loops", tot
print "Promoter-Enhancer", enh
print "Promoter-Promoter", pr
print "Total genes", num_g
print "Other loops", non_enh
