#!/usr/bin/python

import sys
import os

def find(chrid, start, end, fname):
	f = open(fname)
	tmap = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		if ll[0]==chrid:
			ii = ll[3].split("_")
			frag_id = int(ii[2])
			if frag_id>=start and frag_id<=end:
				tmap[frag_id] = int(ll[1]) # or we could go with end ll[2]
	f.close()
	return tmap

def do_one(option="E105", fname=""): #or E135
	f = open(fname)
	#dest = "eryP_eryD_common_genes_50k_interact_observed_none_frag/E105"
	#dest = "eryP_eryD_common_genes_50k_interact_observed_none_frag/E135"
	dest = "."
	i = 1
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		coord = ll[-1].split(";")
		chr_id = ""
		ranges = []
		for ic in coord:
			ii = ic.split("_")
			chr_id = ii[1].split("chr")[1]
			ranges.append(int(ii[2]))
		#coord = ll[0].split("chr")[1]
		#x,y = ll[1], ll[2]
		x = min(ranges)
		y = max(ranges)
		mid = int((x + y)/2.0)
		c_range = chr_id+":"+str(mid-200)+":"+str(mid+200)
		print chr_id+":"+str(mid-200)+":"+str(mid+200)
		gene = ll[-2]
		#option = "E105"
		if option=="E105":
			outfile = "%s/%s.%s.E10.5" % (dest, i, gene)
		elif option=="E135":
			outfile = "%s/%s.%s.E13.5" % (dest, i, gene)
		else:
			print "Error: invalid option"
			sys.exit(0)

		if option=="E105":
			cmd = "java -jar juicer_tools.1.7.6_jcuda.0.8.jar dump observed NONE ~/Downloads/hic_files/20181114_E10_5_Gata1_1029_WC6171_S1_001_mm10.bwt2pairs.validPairs.sorted.hic %s %s FRAG 1 > %s" % (c_range, c_range, outfile)
		elif option=="E135":
			cmd = "java -jar juicer_tools.1.7.6_jcuda.0.8.jar dump observed NONE ~/Downloads/hic_files/20181114_E13_5_Gata1_1029_WC6171_S2_001_mm10.bwt2pairs.validPairs.sorted.hic %s %s FRAG 1 > %s" % (c_range, c_range, outfile)
		os.system(cmd)

		tmap = find("chr"+chr_id, mid-200, mid+200, "mm10_mboI_cut_site_sort.bed")
		f2 = open(outfile)
		f2_w = open(outfile + ".bp", "w")
		for l2 in f2:
			l2 = l2.rstrip("\n")
			ll2 = l2.split("\t")
			f2_w.write("%d\t%d\t%s\n" % (tmap[int(ll2[0])], tmap[int(ll2[1])], ll2[2]))
		f2_w.close()
		f2.close()

		i+=1 
	f.close()

if __name__=="__main__":
	do_one(option="E105", fname=sys.argv[1])
	do_one(option="E135", fname=sys.argv[1])
