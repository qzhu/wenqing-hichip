#for i in `seq 1 100`; do ./get_promoter_anchor_random.sh E13_5_medium/postprocessed_pixels_5000.bedpe Enhancer/Enhancer_DS3_5k_stitched_sorted.bed ../RNA-seq/random.1600/$i|tail -n 4; done > random.E13.5.5k.eryD.spec.genes.txt
#for i in `seq 1 100`; do ./get_promoter_anchor_random.sh E13_5_medium/postprocessed_pixels_10000.bedpe Enhancer/Enhancer_DS3_10k_stitched_sorted.bed ../RNA-seq/random.1600/$i|tail -n 4; done > random.E13.5.10k.eryD.spec.genes.txt
for i in `seq 1 100`; do ./get_promoter_anchor_random.sh E13_5_medium/postprocessed_pixels_25000.bedpe Enhancer/Enhancer_DS3_25k_stitched_sorted.bed ../RNA-seq/random.1600/$i|tail -n 4; done > random.E13.5.25k.eryD.spec.genes.txt

#for i in `seq 1 100`; do ./get_promoter_anchor_random.sh E13_5_medium/postprocessed_pixels_5000.bedpe Enhancer/Enhancer_DS3_5k_stitched_sorted.bed ../RNA-seq/random.1000/$i|tail -n 4; done > random.E13.5.5k.eryP.spec.genes.txt
#for i in `seq 1 100`; do ./get_promoter_anchor_random.sh E13_5_medium/postprocessed_pixels_10000.bedpe Enhancer/Enhancer_DS3_10k_stitched_sorted.bed ../RNA-seq/random.1000/$i|tail -n 4; done > random.E13.5.10k.eryP.spec.genes.txt
for i in `seq 1 100`; do ./get_promoter_anchor_random.sh E13_5_medium/postprocessed_pixels_25000.bedpe Enhancer/Enhancer_DS3_25k_stitched_sorted.bed ../RNA-seq/random.1000/$i|tail -n 4; done > random.E13.5.25k.eryP.spec.genes.txt

#for i in `seq 1 100`; do ./get_promoter_anchor_random.sh E13_5_medium/postprocessed_pixels_5000.bedpe Enhancer/Enhancer_DS3_5k_stitched_sorted.bed ../RNA-seq/random.4000/$i|tail -n 4; done > random.E13.5.5k.common.genes.eryD.gt.eryP.txt
#for i in `seq 1 100`; do ./get_promoter_anchor_random.sh E13_5_medium/postprocessed_pixels_10000.bedpe Enhancer/Enhancer_DS3_10k_stitched_sorted.bed ../RNA-seq/random.4000/$i|tail -n 4; done > random.E13.5.10k.common.genes.eryD.gt.eryP.txt
for i in `seq 1 100`; do ./get_promoter_anchor_random.sh E13_5_medium/postprocessed_pixels_25000.bedpe Enhancer/Enhancer_DS3_25k_stitched_sorted.bed ../RNA-seq/random.4000/$i|tail -n 4; done > random.E13.5.25k.common.genes.eryD.gt.eryP.txt

