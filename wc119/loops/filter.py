#!/usr/bin/python

import sys
import re
import os

f = open(sys.argv[1])
h = f.readline().rstrip("\n").split("\t")
th=float(sys.argv[2])
for l in f:
	l = l.rstrip("\n")
	ll = l.split("\t")
	z = dict(zip(h, ll))
	if float(z["fdrBL"])<th and float(z["fdrDonut"])<th and float(z["fdrH"])<th and float(z["fdrV"])<th:
		print l
f.close()
