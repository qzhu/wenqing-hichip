#!/bin/bash
file1=$1 #E10_5_Gata1_1029/postprocessed_pixels_5000.bedpe
file2=$2 #Enhancer file (BED)
file3=$3 #gene list
cp $file1 /tmp/all.loops.bedpe
cat $file1 |cut -d"	" -f1-3 > /tmp/1
cat $file1 |cut -d"	" -f4-6 > /tmp/2
cat /tmp/1 /tmp/2|sort -u|grep -v "chr"|sed "s/^/chr/g"|sort-bed - > /tmp/all.anchors.bed
bedmap --range 2000 --delim "\t" --skip-unmapped --echo --echo-map-id /tmp/all.anchors.bed mm10.TSS.filtered.sorted.bed |~/filter_gene_assignment.py > /tmp/promoter.overlapped.bed
bedmap --range 1000 --delim "\t" --skip-unmapped --echo --echo-map /tmp/all.anchors.bed $file2 > /tmp/enhancer.overlapped.bed
./classify_loops.py $file3
