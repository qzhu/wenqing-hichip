 2000  ./get_promoter_anchor.sh E10_5_medium/postprocessed_pixels_5000.bedpe Enhancer/Enhancer_PS3_5k_stitched_sorted.bed ../RNA-seq/EryP. \
 2001  vim get_promoter_anchor_random.sh 
 2002  vim do.random.e13.5.sh 
 2003  ls -ltr ../RNA-seq/
 2004  pwd
 2005  vim ../RNA-seq/EryP.specific.lfc1.p001.csv 
 2006  ./get_promoter_anchor.sh E10_5_medium/postprocessed_pixels_5000.bedpe Enhancer/Enhancer_PS3_5k_stitched_sorted.bed ../RNA-seq/EryP.specific.lfc1.p001.csv 
 2007  ./get_promoter_anchor.sh E13_5_medium/postprocessed_pixels_5000.bedpe Enhancer/Enhancer_DS3_5k_stitched_sorted.bed ../RNA-seq/EryP.specific.lfc1.p001.csv 
 2008  vim do.random.e13.5.sh 
 2009  ./get_promoter_anchor.sh E13_5_medium/postprocessed_pixels_5000.bedpe Enhancer/Enhancer_DS3_5k_stitched_sorted.bed ../RNA-seq/EryP.specific.lfc1.p001.csv 
 2010  ./get_promoter_anchor.sh E10_5_medium/postprocessed_pixels_10000.bedpe Enhancer/Enhancer_PS3_10k_stitched_sorted.bed ../RNA-seq/EryP.specific.lfc1.p001.csv 
 2011  ./get_promoter_anchor.sh E13_5_medium/postprocessed_pixels_10000.bedpe Enhancer/Enhancer_DS3_10k_stitched_sorted.bed ../RNA-seq/EryP.specific.lfc1.p001.csv 
 2012  ./get_promoter_anchor.sh E10_5_medium/postprocessed_pixels_25000.bedpe Enhancer/Enhancer_PS3_25k_stitched_sorted.bed ../RNA-seq/EryP.specific.lfc1.p001.csv 
 2013  ./get_promoter_anchor.sh E13_5_medium/postprocessed_pixels_25000.bedpe Enhancer/Enhancer_DS3_25k_stitched_sorted.bed ../RNA-seq/EryP.specific.lfc1.p001.csv 
 2014  ./get_promoter_anchor.sh E10_5_medium/postprocessed_pixels_5000.bedpe Enhancer/Enhancer_PS3_5k_stitched_sorted.bed ../RNA-seq/EryD.specific.lfc1.p001.csv 
 2015  ./get_promoter_anchor.sh E13_5_medium/postprocessed_pixels_5000.bedpe Enhancer/Enhancer_DS3_5k_stitched_sorted.bed ../RNA-seq/EryD.specific.lfc1.p001.csv 
 2016  ./get_promoter_anchor.sh E10_5_medium/postprocessed_pixels_10000.bedpe Enhancer/Enhancer_PS3_10k_stitched_sorted.bed ../RNA-seq/EryD.specific.lfc1.p001.csv 
 2017  ./get_promoter_anchor.sh E13_5_medium/postprocessed_pixels_10000.bedpe Enhancer/Enhancer_DS3_10k_stitched_sorted.bed ../RNA-seq/EryD.specific.lfc1.p001.csv 
 2018  ./get_promoter_anchor.sh E13_5_medium/postprocessed_pixels_25000.bedpe Enhancer/Enhancer_DS3_25k_stitched_sorted.bed ../RNA-seq/EryD.specific.lfc1.p001.csv 
 2019  ./get_promoter_anchor.sh E10_5_medium/postprocessed_pixels_25000.bedpe Enhancer/Enhancer_PS3_25k_stitched_sorted.bed ../RNA-seq/EryD.specific.lfc1.p001.csv 
 2020  ./get_promoter_anchor.sh E10_5_medium/postprocessed_pixels_5000.bedpe Enhancer/Enhancer_PS3_5k_stitched_sorted.bed ../RNA-seq/eryP_eryD_common_genes_eryD_gt_eryP.txt 
 2021  ./get_promoter_anchor.sh E13_5_medium/postprocessed_pixels_5000.bedpe Enhancer/Enhancer_DS3_5k_stitched_sorted.bed ../RNA-seq/eryP_eryD_common_genes_eryD_gt_eryP.txt 
 2022  ./get_promoter_anchor.sh E10_5_medium/postprocessed_pixels_10000.bedpe Enhancer/Enhancer_PS3_10k_stitched_sorted.bed ../RNA-seq/eryP_eryD_common_genes_eryD_gt_eryP.txt 
 2023  ./get_promoter_anchor.sh E13_5_medium/postprocessed_pixels_10000.bedpe Enhancer/Enhancer_DS3_10k_stitched_sorted.bed ../RNA-seq/eryP_eryD_common_genes_eryD_gt_eryP.txt 
 2024  ./get_promoter_anchor.sh E10_5_medium/postprocessed_pixels_25000.bedpe Enhancer/Enhancer_PS3_25k_stitched_sorted.bed ../RNA-seq/eryP_eryD_common_genes_eryD_gt_eryP.txt 
 2025  ./get_promoter_anchor.sh E13_5_medium/postprocessed_pixels_25000.bedpe Enhancer/Enhancer_DS3_25k_stitched_sorted.bed ../RNA-seq/eryP_eryD_common_genes_eryD_gt_eryP.txt 
 2026  ls -ltr
 2027  ./calculate_loop_random.py 
 2028  ./calculate_loop_random.py random.E10.5.5k.eryP.spec.genes.txt random.E13.5.5k.eryP.spec.genes.txt 
 2029* ./calculate_loop_random.py random.E10.5.5k.eryP.spec.genes.txt random.E13.5.5k.eryP.spec.genes.txt 
 2030  ./calculate_loop_random.py random.E10.5.5k.eryP.spec.genes.txt random.E13.5.5k.eryP.spec.genes.txt 0.005
 2031  ./calculate_loop_random.py random.E10.5.5k.eryP.spec.genes.txt random.E13.5.5k.eryP.spec.genes.txt -0.005
 2032  vim calculate_loop_random.py 
 2033  ./calculate_loop_random.py random.E10.5.10k.eryP.spec.genes.txt random.E13.5.10k.eryP.spec.genes.txt -0.006
 2034  ./calculate_loop_random.py random.E10.5.25k.eryP.spec.genes.txt random.E13.5.25k.eryP.spec.genes.txt 0.011
 2035  ./calculate_loop_random.py random.E10.5.5k.eryD.spec.genes.txt random.E13.5.5k.eryD.spec.genes.txt 0.08899
 2036  ./calculate_loop_random.py random.E10.5.10k.eryD.spec.genes.txt random.E13.5.10k.eryD.spec.genes.txt 0.102999
 2037  ./calculate_loop_random.py random.E10.5.25k.eryD.spec.genes.txt random.E13.5.25k.eryD.spec.genes.txt 0.179
 2038  ./calculate_loop_random.py random.E10.5.5k.common.genes.eryD.gt.eryP.txt random.E13.5.5k.common.genes.eryD.gt.eryP.txt 0.056999
 2039  vim calculate_loop_random.py 
 2040  vim random.E10.5.5k.common.genes.eryD.gt.eryP.txt
 2041  ./calculate_loop_random.py random.E10.5.5k.eryP.spec.genes.txt random.E13.5.5k.eryP.spec.genes.txt 0.005
 2042  ./calculate_loop_random.py random.E10.5.10k.eryP.spec.genes.txt random.E13.5.10k.eryP.spec.genes.txt -0.006
 2043  ./calculate_loop_random.py random.E10.5.25k.eryP.spec.genes.txt random.E13.5.25k.eryP.spec.genes.txt 0.011
 2044  ./calculate_loop_random.py random.E10.5.5k.eryD.spec.genes.txt random.E13.5.5k.eryD.spec.genes.txt 0.08899
 2045  ./calculate_loop_random.py random.E10.5.10k.eryD.spec.genes.txt random.E13.5.10k.eryD.spec.genes.txt 0.102999
 2046  ./calculate_loop_random.py random.E10.5.25k.eryD.spec.genes.txt random.E13.5.25k.eryD.spec.genes.txt 0.179
 2047  ./calculate_loop_random.py random.E10.5.5k.common.genes.eryD.gt.eryP.txt random.E13.5.5k.common.genes.eryD.gt.eryP.txt 0.056999
 2048  ./calculate_loop_random.py random.E10.5.10k.common.genes.eryD.gt.eryP.txt random.E13.5.10k.common.genes.eryD.gt.eryP.txt 0.073999
 2049  ./calculate_loop_random.py random.E10.5.25k.common.genes.eryD.gt.eryP.txt random.E13.5.25k.common.genes.eryD.gt.eryP.txt 0.068
 2050  ./calculate_loop_random.py random.E10.5.5k.common.genes.eryD.gt.eryP.txt random.E13.5.5k.common.genes.eryD.gt.eryP.txt 0.057
 2051  ./calculate_loop_random.py random.E10.5.5k.common.genes.eryD.gt.eryP.txt random.E13.5.5k.common.genes.eryD.gt.eryP.txt 0.06
 2052  history
 2053  history > script.compare.mar15.2019.sh
