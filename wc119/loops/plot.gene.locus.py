#!/usr/bin/python
import numpy as np
import sys
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy
import scipy.stats
import re
import math
import pandas as pd
import seaborn as sns

def get_common_start_end(n1, n2):
	f = open(n1)
	all_c = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		all_c.append(c1)
		all_c.append(c2)
	f.close()
	f = open(n2)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		all_c.append(c1)
		all_c.append(c2)
	f.close()
	start = min(all_c)
	end = max(all_c)
	return start, end

def read_file(n, start, end, t_direction):
	f = open(n)
	num_row = (end-start)/1+1
	interact = np.zeros((num_row, num_row), dtype="float32")
	f = open(n)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		val = float(ll[2])
		ind1 = (c1 - start)/1
		ind2 = (c2 - start)/1
		if t_direction=="-":
			ind1 = num_row - 1 - ind1
			ind2 = num_row - 1 - ind2
		interact[ind1, ind2] = val
		interact[ind2, ind1] = val
	f.close()
	return interact

def convert_matrix(a_mat):
	aX, aY, aV = [], [], []
	for i in range(a_mat.shape[0]):
		for j in range(a_mat.shape[1]):
			if a_mat[i,j]>0:
				#alist.append((i, j, a_mat[i,j]))
				aX.append(i)
				aY.append(j)
				aV.append(a_mat[i,j])
	return np.array(aX), np.array(aY), np.array(aV)

t_file = sys.argv[1]
start,end = get_common_start_end(t_file + ".E10.5", t_file + ".E13.5")
interact105 = read_file(t_file+".E10.5", start, end, "+")
interact = read_file(t_file+".E13.5", start, end, "+")

i105_X, i105_Y, i105_V = convert_matrix(interact105)
i135_X, i135_Y, i135_V = convert_matrix(interact)

size_factor = 10
colormap="Reds"
f,axn = plt.subplots(1,2,figsize=(size_factor*2,size_factor))
plt.subplots_adjust(hspace=0.01, wspace=0.01)
cm = plt.cm.get_cmap(colormap)

#ax = sns.heatmap(pd.DataFrame(interact105), annot=False, fmt=".2f", ax=axn.flat[0], cmap=colormap)
#ax = sns.heatmap(pd.DataFrame(interact), annot=False, fmt=".2f", ax=axn.flat[1], cmap=colormap)

axn.flat[0].scatter(i105_X, i105_Y*-1, s=i105_V, alpha=0.3)
axn.flat[1].scatter(i135_X, i135_Y*-1, s=i135_V, alpha=0.3)

plt.show()
'''
mat_135 = np.zeros((201, 201), dtype="float32")
mat_105 = np.zeros((201, 201), dtype="float32")
mat_135 = np.copy(mat201_135)
mat_105 = np.copy(mat201_105)
'''
