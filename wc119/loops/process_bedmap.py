#!/usr/bin/python

import sys

#f = open(sys.stdin)
f = sys.stdin
gene_id = False
gid = ""
for l in f:
	if l=="": break
	l = l.rstrip("\n")
	ll = l.split("\t")
	chrm = ll[0]
	start = ll[1]
	end = ll[2]
	if ll[3]==ll[-1]:
		gene_id = False
	else:
		gene_id = True
		gid = ll[3]
	mapped = ll[-1].split(";")
	dd = []
	for mm in mapped:
		tx = mm.split("_")
		tid = tx[2]
		dd.append(int(tid))
	t_chrm = chrm.split("chr")[1]
	if not gene_id:
		print chrm, start, end, "%s:%d:%d" % (t_chrm, min(dd), max(dd))
	else:
		print chrm, start, end, gid, "%s:%d:%d" % (t_chrm, min(dd), max(dd))
f.close()
