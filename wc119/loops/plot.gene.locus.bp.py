#!/usr/bin/python
import numpy as np
import sys
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy
import scipy.stats
import re
import math
import pandas as pd
import seaborn as sns

def get_common_start_end(n1, n2):
	f = open(n1)
	all_c = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		all_c.append(c1)
		all_c.append(c2)
	f.close()
	f = open(n2)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		all_c.append(c1)
		all_c.append(c2)
	f.close()
	start = min(all_c)
	end = max(all_c)
	return start, end

def read_file(n, start, end, t_direction):
	f = open(n)
	interact = set([])
	f = open(n)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		val = float(ll[2])
		interact.add((c1, c2, val))
		interact.add((c2, c1, val))
	f.close()
	aX, aY, aV = [], [], []
	for i,j,k in interact:
		aX.append(i)
		aY.append(j)
		aV.append(k)
	return np.array(aX), np.array(aY), np.array(aV)

t_file = sys.argv[1]
start,end = get_common_start_end(t_file + ".E10.5.bp", t_file + ".E13.5.bp")
print start, end
i105_X, i105_Y, i105_V = read_file(t_file+".E10.5.bp", start, end, "+")
i135_X, i135_Y, i135_V = read_file(t_file+".E13.5.bp", start, end, "+")

#i105_X, i105_Y, i105_V = convert_matrix(interact105)
#i135_X, i135_Y, i135_V = convert_matrix(interact)

print np.percentile(i105_V, 95)
print np.percentile(i135_V, 95)

size_factor = 10
colormap="Reds"
f,axn = plt.subplots(1,2,figsize=(size_factor*2,size_factor))
plt.subplots_adjust(hspace=0.01, wspace=0.01)
cm = plt.cm.get_cmap(colormap)

#ax = sns.heatmap(pd.DataFrame(interact105), annot=False, fmt=".2f", ax=axn.flat[0], cmap=colormap)
#ax = sns.heatmap(pd.DataFrame(interact), annot=False, fmt=".2f", ax=axn.flat[1], cmap=colormap)

axn.flat[0].scatter(i105_X, i105_Y*-1, s=i105_V, alpha=0.5, c="r")
axn.flat[1].scatter(i135_X, i135_Y*-1, s=i135_V, alpha=0.5, c="r")

'''
axn.flat[0].set_yticklabels([])
axn.flat[0].set_xticklabels([])

axn.flat[1].set_yticklabels([])
axn.flat[1].set_xticklabels([])
'''
plt.show()


e105_val = []
print "E105"
for i in range(i105_X.shape[0]):
	#if i105_X[i]>111497000 and i105_X[i]<111511000: #Eif5
	if i105_X[i]>46401800 and i105_X[i]<46408000: #hemgn version 2
	#if i105_X[i]>46402000 and i105_X[i]<46410500: #hemgn
	#if i105_X[i]>46406000 and i105_X[i]<46410000: #hemgn version 1
		#print i105_X[i], i105_Y[i], i105_V[i]
		for j in range(int(i105_V[i])):
			e105_val.append(i105_Y[i])

e135_val = []
print "E135"
for i in range(i135_X.shape[0]):
	#if i135_X[i]>111497000 and i135_X[i]<111511000: #Eif5
	if i135_X[i]>46401800 and i135_X[i]<46408000: #hemgn
	#if i135_X[i]>46402000 and i135_X[i]<46410500: #hemgn
	#if i135_X[i]>46406000 and i135_X[i]<46410000: #hemgn version 1
		#print i135_X[i], i135_Y[i], i135_V[i]
		for j in range(int(i135_V[i])):
			e135_val.append(i135_Y[i])

e105_val = np.array(e105_val)
e135_val = np.array(e135_val)

hist_max = max(np.max(e105_val), np.max(e135_val))
hist_min = min(np.min(e105_val), np.min(e135_val))

fig, axs = plt.subplots(2,1, sharex=True, sharey=True, tight_layout=True)
n, bins, _ = axs[0].hist(e105_val, bins=200)
n2, bins2, _ = axs[1].hist(e135_val, bins=200)

for i in range(n.shape[0]):
	mid = (bins[i] + bins[i+1])/2.0
	print int(mid), n[i], "E10.5"

for i in range(n2.shape[0]):
	mid = (bins2[i] + bins2[i+1])/2.0
	print int(mid), n2[i], "E13.5"


plt.show()


'''
mat_135 = np.zeros((201, 201), dtype="float32")
mat_105 = np.zeros((201, 201), dtype="float32")
mat_135 = np.copy(mat201_135)
mat_105 = np.copy(mat201_105)
'''
