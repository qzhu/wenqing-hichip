#!/bin/bash
range=$1
tss=$2
output=$3
#java -jar juicer_tools.1.7.6_jcuda.0.8.jar dump oe VC_SQRT ~/Downloads/hic_files/20181114_E10_5_Gata1_1029_WC6171_S1_001_mm10.bwt2pairs.validPairs.sorted.hic $range $range FRAG 1|./get_4c.py $tss 1 > /tmp/id.1.e10.5.txt
#java -jar juicer_tools.1.7.6_jcuda.0.8.jar dump oe VC_SQRT ~/Downloads/hic_files/20181114_E13_5_Gata1_1029_WC6171_S2_001_mm10.bwt2pairs.validPairs.sorted.hic $range $range FRAG 1|./get_4c.py $tss 1 > /tmp/id.1.e13.5.txt
java -jar juicer_tools.1.7.6_jcuda.0.8.jar dump observed NONE ~/Downloads/hic_files/20181114_E10_5_Gata1_1029_WC6171_S1_001_mm10.bwt2pairs.validPairs.sorted.hic $range $range FRAG 1|./get_4c.py $tss 1 > /tmp/id.1.e10.5.txt
java -jar juicer_tools.1.7.6_jcuda.0.8.jar dump observed NONE ~/Downloads/hic_files/20181114_E13_5_Gata1_1029_WC6171_S2_001_mm10.bwt2pairs.validPairs.sorted.hic $range $range FRAG 1|./get_4c.py $tss 1 > /tmp/id.1.e13.5.txt
paste /tmp/id.1.e10.5.txt /tmp/id.1.e13.5.txt > $output

