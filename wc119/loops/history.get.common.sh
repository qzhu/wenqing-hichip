 2012  cat eryP_eryD_common_genes_eryD_gt_eryP.txt|cut -d"	" -f2 > eryP_eryD_common_genes_eryD_gt_eryP.genes
 2016  for i in `cat eryP_eryD_common_genes_eryD_gt_eryP.genes`; do cat mm10.TSS.filtered.sorted.bed |grep "	$i	"; done > /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.bed
 2018  sort-bed /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.bed > /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed
 2021  bedops -e 1 H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |wc
 2022  bedops -e 1 H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |less
 2023  bedmap  H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |less
 2025  bedmap --echo-map --echo-ref H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |less
 2026  bedmap --echo-map --echo H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |less
 2027  bedmap --echo-map-id --echo H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |less
 2028  bedmap --echo --echo-map-id H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |less
 2029  bedmap --echo --echo-map-id --skip-unmapped H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |less
 2030  bedmap --echo --echo-map-id --skip-unmapped --sep="\t" H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |less
 2031  bedmap --echo --echo-map-id --skip-unmapped --sep "\t" H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |less
 2032  bedmap --echo --echo-map-id --skip-unmapped --delim="\t" H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |less
 2033  bedmap
 2034  bedmap --echo --echo-map-id --skip-unmapped --delim "\t" H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |less
 2035  bedmap --echo --echo-map-id --skip-unmapped --delim "\t" H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |~/filter_gene_assignment.py 
 2036  bedmap --echo --echo-map-id --skip-unmapped --delim "\t" H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |~/filter_gene_assignment.py |less
 2037  bedmap --echo --echo-map-id --skip-unmapped --delim "\t" H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |~/filter_gene_assignment.py > /tmp/H3K4me3_E105_common_genes.sort.bed
 2038  bedmap --echo --echo-map-id --skip-unmapped --delim "\t" H3K4me3_E135_WC12WC13_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryD_gt_eryP.genes.TSS.sort.bed |~/filter_gene_assignment.py > /tmp/H3K4me3_E135_common_genes.sort.bed
 2039  vim /tmp/H3K4me3_E105_common_genes.sort.bed 
 2040  bedmap --echo --echo-map --skip-unmapped /tmp/H3K4me3_E105_common_genes.sort.bed /tmp/H3K4me3_E105_common_genes.sort.bed 
 2041  bedmap --echo --echo-map --skip-unmapped /tmp/H3K4me3_E135_common_genes.sort.bed /tmp/H3K4me3_E105_common_genes.sort.bed 
 2042  bedmap --echo --echo-map --skip-unmapped /tmp/H3K4me3_E135_common_genes.sort.bed /tmp/H3K4me3_E105_common_genes.sort.bed |less
 2043  bedmap --echo --echo-map --skip-unmapped /tmp/H3K4me3_E135_common_genes.sort.bed /tmp/H3K4me3_E105_common_genes.sort.bed |wc
 2044  bedmap --echo --echo-map --skip-unmapped --delim "\t" /tmp/H3K4me3_E135_common_genes.sort.bed /tmp/H3K4me3_E105_common_genes.sort.bed > /tmp/H3K4me3_E135_E105_common_genes.sort.bed
 2045  vim /tmp/H3K4me3_E135_E105_common_genes.sort.bed
 2046  cat /tmp/H3K4me3_E135_E105_common_genes.sort.bed|cut -d"	" -f1,2,3,4|less
 2047  cat /tmp/H3K4me3_E135_E105_common_genes.sort.bed|cut -d"	" -f1,2,3,4 > /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed
 2048  cat /tmp/H3K4me3_E135_E105_common_genes.sort.bed|cut -d"	" -f13,14,15,16 > /tmp/H3K4me3_E135_E105_common_genes.right.sort.bed
 2049  vim /tmp/H3K4me3_E135_E105_common_genes.sort.bed
 2050  vim /tmp/H3K4me3_E135_E105_common_genes.right.sort.bed 
 2051  cat /tmp/H3K4me3_E135_E105_common_genes.sort.bed|cut -d"	" -f13,14,15,16 > /tmp/H3K4me3_E135_E105_common_genes.right.sort.bed
 2052  vim /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed 
 2053  cat /tmp/H3K4me3_E135_E105_common_genes.sort.bed|cut -d"	" -f1,2,3,4,11 > /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed
 2054  vim /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed 
 2055  cat /tmp/H3K4me3_E135_E105_common_genes.sort.bed|cut -d"	" -f12,13,14,15,22 > /tmp/H3K4me3_E135_E105_common_genes.right.sort.bed
 2056  vim /tmp/H3K4me3_E135_E105_common_genes.right.sort.bed 
 2057  ls -ltr
 2058  bedmap /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed /tmp/Gata1_DS3_sort.bed
 2059  bedmap --range 50000 /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed /tmp/Gata1_DS3_sort.bed
 2060  bedmap --echo --echo-map --range 50000 /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed /tmp/Gata1_DS3_sort.bed
 2061  bedmap --echo --echo-map --range 50000 /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed Gata1_DS3_sort.bed
 2062  bedmap --echo --echo-map-id --range 50000 /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed Gata1_DS3_sort.bed
 2063  bedmap --echo --echo-map-id --skip-unmapped --range 50000 /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed Gata1_DS3_sort.bed
 2064  bedmap --echo --echo-map-id --skip-unmapped --range 50000 /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed Gata1_DS3_sort.bed|wc
 2065  bedmap --echo --echo-map-id --skip-unmapped --range 50000 --delim "\t" /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed Gata1_DS3_sort.bed|wc
 2066  bedmap --echo --echo-map-id --skip-unmapped --range 50000 --delim "\t" /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed Gata1_DS3_sort.bed
 2067  bedmap --echo --echo-map-id --skip-unmapped --range 50000 --delim "\t" /tmp/H3K4me3_E135_E105_common_genes.left.sort.bed Gata1_DS3_sort.bed > /tmp/H3K4me3_E135_E105_common_genes.left.Gata1_DS3.bed
 2068  vim /tmp/H3K4me3_E135_E105_common_genes.left.Gata1_DS3.bed
 2069  bedmap --echo --echo-map-id --skip-unmapped Gata1_DS3_sort.bed Gata1_PS3_sort.bed 
 2070  bedmap --echo-id --echo-map-id --skip-unmapped Gata1_DS3_sort.bed Gata1_PS3_sort.bed 
 2071  bedmap --echo --echo-map-id --skip-unmapped --delim "\t" Gata1_DS3_sort.bed Gata1_PS3_sort.bed 
 2072  bedmap --echo --echo-map-id --skip-unmapped --delim "\t" Gata1_DS3_sort.bed Gata1_PS3_sort.bed |cut -d"	" -f4,11
 2073  bedmap --echo --echo-map-id --skip-unmapped --delim "\t" Gata1_DS3_sort.bed Gata1_PS3_sort.bed |cut -d"	" -f4,11|wc
 2074  bedmap --echo --echo-map-id --skip-unmapped --delim "\t" Gata1_DS3_sort.bed Gata1_PS3_sort.bed |cut -d"	" -f4,11 > GATA1_DS3_and_PS3_map.bed
 2075  vim GATA1_DS3_and_PS3_map.bed
 2076  ls -ltr /tmp
 2077  vim /tmp/H3K4me3_E135_E105_common_genes.left.Gata1_DS3.bed
 2078  bedmap --echo --echo-map-id --skip-unmapped --range 50000 --delim "\t" /tmp/H3K4me3_E135_E105_common_genes.right.sort.bed Gata1_PS3_sort.bed > /tmp/H3K4me3_E135_E105_common_genes.right.Gata1_PS3.bed
 2079  ls -ltr
 2080  ls -ltr /tmp
 2081  vim /tmp/H3K4me3_E135_E105_common_genes.right.Gata1_PS3.bed
 2082  ls -ltr
 2083  ls -ltr /tmp
 2084  vim /tmpH3K4me3_E135_E105_common_genes.right.Gata1_PS3.bed
 2085  vim /tmp/H3K4me3_E135_E105_common_genes.right.Gata1_PS3.bed
 2086  vim do.get.overlap.py
 2087  chmod a+x do.get.overlap.py 
 2088  ./do.get.overlap.py 
 2177  ./do.get.overlap.py |wc
 2178  ./do.get.overlap.py |less
 2179  ./do.get.overlap.py |sort-bed -
 2180  ./do.get.overlap.py |sort-bed - > /tmp/H3K4me3_E135_E105_common_genes.Gata1.mapped.demo.genes.bed
 2182  bedops -u --range 50000 /tmp/H3K4me3_E135_E105_common_genes.Gata1.mapped.demo.genes.bed > /tmp/H3K4me3_E135_E105_common_genes.Gata1.mapped.demo.genes.pad50k.bed
 2183  vim /tmp/H3K4me3_E135_E105_common_genes.Gata1.mapped.demo.genes.pad50k.bed
 2184  vim do_dump.py
 2185  chmod a+x do_dump.py 
 2186  vim do_dump.py
 2187  ./do_dump.py /tmp/H3K4me3_E135_E105_common_genes.Gata1.mapped.demo.genes.pad50k.bed
 2188  vim do_dump.py
 2189  history
 2190  vim get_dump.sh 
 2191  vim do_dump.py
 2192  chmod a+x do_dump.py 
 2193  ./do_dump.py /tmp/H3K4me3_E135_E105_common_genes.Gata1.mapped.demo.genes.pad50k.bed
 2194  vim do_dump.py
 2195  ./do_dump.py /tmp/H3K4me3_E135_E105_common_genes.Gata1.mapped.demo.genes.pad50k.bed
 2196  vim do_dump.py
 2197  ./do_dump.py /tmp/H3K4me3_E135_E105_common_genes.Gata1.mapped.demo.genes.pad50k.bed
 2198  ls -ltr
 2199  vim get_dump.sh 
 2200  vim do_dump.py 
 2201  mkdir eryP_eryD_common_genes_50k_interact/E135
 2202  mkdir eryP_eryD_common_genes_50k_interact/E105
 2203  ./do_dump.py /tmp/H3K4me3_E135_E105_common_genes.Gata1.mapped.demo.genes.pad50k.bed
 2204  mkdir eryP_eryD_common_genes_50k_interact
 2205  mkdir eryP_eryD_common_genes_50k_interact/E105
 2206  mkdir eryP_eryD_common_genes_50k_interact/E135
 2207  ls -ltr
 2208  ./do_dump.py 
 2209  ./do_dump.py /tmp/H3K4me3_E135_E105_common_genes.Gata1.mapped.demo.genes.pad50k.bed 
 2210  vim mm10.TSS.filtered.sorted.bed 
 2211  ./do_dump.py /tmp/H3K4me3_E135_E105_common_genes.Gata1.mapped.demo.genes.pad50k.bed 
 2212  history

