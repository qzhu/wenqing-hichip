

#cat ../RNA-seq/EryP.specific.lfc1.p001.csv|sort -t"	" -g -k10|head -n 170|cut -d"	" -f2 > /tmp/eryP.specific.genes.txt
cat $1
for i in `cat $1`; do cat mm10.TSS.filtered.sorted.bed |grep "	$i	"; done |sort-bed - > 400frag/random.3.specific.genes.mapped.bed
bedmap --echo --echo-map-id --skip-unmapped --delim "\t" H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak 400frag/random.3.specific.genes.mapped.bed|~/filter_gene_assignment.py > 400frag/random.3.H3K4me3.marked.bed
cat 400frag/random.3.H3K4me3.marked.bed |cut -d"	" -f1,2,3,11 > 400frag/random.3.H3K4me3.marked.genes.bed
bedmap --echo --echo-map-id --delim "\t" 400frag/random.3.H3K4me3.marked.genes.bed ~/wc119/loops/mm10_mboI_cut_site_sort.bed > 400frag/random.3.H3K4me3.marked.genes.frag.bed
