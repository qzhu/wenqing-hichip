
cat ../RNA-seq/EryP.specific.lfc1.p001.csv|sort -t"	" -g -k10|head -n 170|cut -d"	" -f2 > /tmp/eryP.specific.genes.txt
for i in `cat /tmp/eryP.specific.genes.txt`; do cat mm10.TSS.filtered.sorted.bed |grep "	$i	"; done |sort-bed - > /tmp/eryP.specific.genes.mapped.bed
bedmap --echo --echo-map-id --skip-unmapped --delim "\t" H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP.specific.genes.mapped.bed|~/filter_gene_assignment.py > /tmp/eryP.H3K4me3.marked.bed
cat /tmp/eryP.H3K4me3.marked.bed |cut -d"	" -f1,2,3,11 > /tmp/eryP.H3K4me3.marked.genes.bed
bedmap --echo --echo-map-id --delim "\t" /tmp/eryP.H3K4me3.marked.genes.bed ~/wc119/loops/mm10_mboI_cut_site_sort.bed > /tmp/eryP.H3K4me3.marked.genes.frag.bed
#./do_dump_gene_locus_frag.py other.loci.sort.frag.bed
./do_dump_gene_locus_frag.py /tmp/eryP.H3K4me3.marked.genes.frag.bed
