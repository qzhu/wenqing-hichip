#!/usr/bin/python

import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy
import scipy.stats
import sys
import re
import os
import numpy as np
import math
import pandas as pd
import seaborn as sns
from scipy.spatial.distance import squareform, pdist
from scipy.stats import percentileofscore

def read(n):
	f = open(n)
	d1, d2 = [], []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		e10_5 = ll[0].split()
		e13_5 = ll[1].split()
		d1.append((int(e10_5[1]), float(e10_5[3])))
		d2.append((int(e13_5[1]), float(e13_5[3])))
	f.close()
	return d1, d2

if __name__=="__main__":
	d1, d2 = read(sys.argv[1])
	x1 = int(sys.argv[2])
	x2 = int(sys.argv[3])
	d1_x = [t[0] for t in d1]
	d1_y = [t[1] for t in d1]
	d2_x = [t[0] for t in d2]
	d2_y = [t[1] for t in d2]
	#ax = plt.gca()
	#ax.get_xaxis().get_major_formatter().set_scientific(False)
	plt.xlim(x1, x2)
	plt.plot(d1_x, d1_y, "b-", alpha=0.7, linewidth=0.5)
	plt.plot(d2_x, d2_y, "r-", alpha=0.7, linewidth=0.5)
	plt.show()

