#!/usr/bin/python

import sys
import re
import os

f = open(sys.argv[1])
by_coord = {}
for l in f:
	l = l.rstrip("\n")
	ll = l.split("\t")
	c1, c2 = int(ll[0]), int(ll[1])
	val = float(ll[2])
	by_coord.setdefault(c1, [])
	by_coord.setdefault(c2, [])
	if c1==c2:
		by_coord[c1].append((c2, val))
	else:
		by_coord[c1].append((c2, val))
		by_coord[c2].append((c1, val))
f.close()

interest = int(sys.argv[2])
chrm = sys.argv[3]

for c in by_coord:
	by_coord[c].sort()

for i,j in by_coord[interest]:
	sys.stdout.write("%s\t%d\t%d\t%.5f\n" % (chrm, i, i+4999, j))
