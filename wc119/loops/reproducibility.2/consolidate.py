#!/usr/bin/python

import sys
import os
import re

def read_file(n):
	f = open(n)
	alist = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1 = int(ll[0])
		c2 = int(ll[1])
		val = float(ll[2])
		alist.append((c1, c2, val))
	f.close()
	return alist

def do_consolidate(folder):
	#os.chdir(folder)

	all_interact = {}
	for files in os.listdir(folder):
		#print files
		ii = files.split(".")[1].split("-")
		chr_id = ii[0]
		alist = read_file(folder+"/"+files)
		for i,j,k in alist:
			all_interact[(chr_id, i, j)] = k
			all_interact[(chr_id, j, i)] = k
		#	print "chr"+chr_id, i, j, k
	return all_interact


#all_interact_orig = do_consolidate("orig.E13.5/25000")
#all_interact_rep = do_consolidate("rep.E13.5.comb/25000")
all_interact_orig = do_consolidate("orig.E10.5/25000")
all_interact_rep = do_consolidate("rep.E10.5/25000")
step=25000

f = open(sys.argv[1]) #postprocessed
f.readline()
for l in f:
	l = l.rstrip("\n")
	ll = l.split("\t")
	q = (ll[0], int(ll[1]), int(ll[4]))
	qx = []
	qx.append(q)
	#for i in [-1*step, 0, step]:
	#	for j in [-1*step, 0, step]:
	#		qx.append((q[0], q[1]+i, q[2]+j))
	#for q1 in qx:
	#	if all_interact_orig.has_key(q1) and all_interact_rep.has_key(q1):
	#		print all_interact_orig[q1], all_interact_rep[q1]
	if all_interact_orig.has_key(q) and all_interact_rep.has_key(q):
			print all_interact_orig[q], all_interact_rep[q]
	
