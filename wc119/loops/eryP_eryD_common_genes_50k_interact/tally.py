#!/usr/bin/python
import numpy as np
import sys
import os

def get_common_start_end(n1, n2):
	f = open(n1)
	all_c = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		all_c.append(c1)
		all_c.append(c2)
	f.close()
	f = open(n2)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		all_c.append(c1)
		all_c.append(c2)
	f.close()
	start = min(all_c)
	end = max(all_c)
	return start, end

def read_file(n, start, end):
	f = open(n)
	num_row = (end-start)/5000+1
	interact = np.zeros((num_row, num_row), dtype="float32")
	f = open(n)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		val = float(ll[2])
		ind1 = (c1 - start)/5000
		ind2 = (c2 - start)/5000
		interact[ind1, ind2] = val
		interact[ind2, ind1] = val
	f.close()
	return interact

os.chdir("E135")

#20
mat20_135 = np.zeros((20, 20), dtype="float32")
mat20_105 = np.zeros((20, 20), dtype="float32")
num20 = 0

#21
mat21_135 = np.zeros((21, 21), dtype="float32")
mat21_105 = np.zeros((21, 21), dtype="float32")
num21 = 0

for t_file in os.listdir("."):
	print t_file
	start,end = get_common_start_end(t_file, "../E105/%s" % t_file)
	interact = read_file(t_file, start, end)
	interact105 = read_file("../E105/%s" % t_file, start, end)

	#if start!=start105 or end!=end105:
	#	print "BAD"
	print interact.shape[0], interact105.shape[0]

	if interact.shape[0]==20:
		mat20_135 = np.add(mat20_135, interact)
		mat20_105 = np.add(mat20_105, interact105)
		num20+=1
	elif interact.shape[0]==21:
		mat21_135 = np.add(mat21_135, interact)
		mat21_105 = np.add(mat21_105, interact105)
		num21+=1

mat20_135 = np.divide(mat20_135, float(num20))
mat20_105 = np.divide(mat20_105, float(num20))

mat21_135 = np.divide(mat21_135, float(num21))
mat21_105 = np.divide(mat21_105, float(num21))

#print mat20_135

print "MAT21_135"
for i in range(mat21_135.shape[0]):
	for j in range(mat21_135.shape[1]):
		print mat21_135[i,j],
	print ""

print "MAT21_105"
for i in range(mat21_105.shape[0]):
	for j in range(mat21_105.shape[1]):
		print mat21_105[i,j],
	print ""

#print mat20_105	
