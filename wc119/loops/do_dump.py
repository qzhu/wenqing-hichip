#!/usr/bin/python

import sys
import os

f = open(sys.argv[1])
dest = "eryP_eryD_common_genes_50k_interact/E135"
#dest = "eryP_eryD_common_genes_50k_interact/E105"
i = 1
for l in f:
	l = l.rstrip("\n")
	ll = l.split("\t")
	coord = ll[0].split("chr")[1]
	x,y = ll[1], ll[2]
	c_range = coord+":"+x+":"+y
	print coord+":"+x+":"+y
	gene = ll[-1].split()[1]
	#cmd = "java -jar juicer_tools.1.7.6_jcuda.0.8.jar dump observed VC_SQRT ~/Downloads/hic_files/20181114_E10_5_Gata1_1029_WC6171_S1_001_mm10.bwt2pairs.validPairs.sorted.hic %s %s BP 5000 > %s/%s.%s" % (c_range, c_range, dest, i, gene)
	cmd = "java -jar juicer_tools.1.7.6_jcuda.0.8.jar dump observed VC_SQRT ~/Downloads/hic_files/20181114_E13_5_Gata1_1029_WC6171_S2_001_mm10.bwt2pairs.validPairs.sorted.hic %s %s BP 5000 > %s/%s.%s" % (c_range, c_range, dest, i, gene)
	#print cmd
	os.system(cmd)
	i+=1 
f.close()
