#!/usr/bin/python
import sys
import re
import os
import numpy as np
def read_peak_id_to_coord_map(n):
	f = open(n)
	tt = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, s1, s2 = ll[0], int(ll[1]), int(ll[2])
		t_id = ll[3]
		tt[t_id] = (c1, s1, s2)
	f.close()
	return tt

def read_mapping(n, peak_id_to_coord):
	f = open(n)
	tt = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, s1, s2 = ll[0], int(ll[1]), int(ll[2])
		g = ll[3]
		enh = ll[4].split(";")
		tt.setdefault(g, {})
		tt[g]["promoter"] = (c1, s1, s2)
		tt[g]["enhancer"] = []
		for e in enh:
			tt[g]["enhancer"].append(peak_id_to_coord[e])
	f.close()
	return tt

def read_interact(n):
	f = open(n)
	tt = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		tt.append((int(ll[0]), int(ll[1]), float(ll[2])))
	f.close()
	return tt

if __name__=="__main__":
	peak_map = read_peak_id_to_coord_map("/home/qzhu/wc119/loops/Gata1_PS3.bed")
	tt = read_mapping("/tmp/eryP.enhancer.to.promoter.mapping.bed", peak_map)
	
	by_g_e10 = {}
	by_g_e13 = {}
	os.chdir("/home/qzhu/wc119/loops/eryP.specific.loops")
	for fname in os.listdir("."):
		if os.path.isfile(fname):
			xx = fname.split(".")
			gname = xx[1]
			if fname.endswith("E10.5.bp"):
				by_g_e10[gname] = read_interact(fname)
			if fname.endswith("E13.5.bp"):
				by_g_e13[gname] = read_interact(fname)

	bg = []
	for g in sorted(by_g_e13.keys()):
		interact = by_g_e13[g]
		if g not in tt:
			continue
		t_val = {}
		for ind,(l,m,n) in enumerate(tt[g]["enhancer"]):
			t_val.setdefault(ind, 0)
		background = []
		for i,j,k in interact:
			#print "interaction", i, j, k
			promoter = -1 # 0 means i is promoter, 1 means j is promoter
			pro, enh = tt[g]["promoter"], tt[g]["enhancer"]
			pro_1, pro_2 = pro[1], pro[2] #_1 is start, _2 is end
			if i>=pro_1-1000 and i<=pro_2+1000:
				promoter = 0
			elif j>=pro_1-1000 and j<=pro_2+1000:
				promoter = 1

			if promoter==-1:
				continue
			if promoter==0:
				for ind,(l,m,n) in enumerate(enh):
					enh_1, enh_2 = m, n
					if j>=enh_1-1000 and j<=enh_2+1000:
						t_val[ind] += k
					else:
						background.append(k)
			elif promoter==1:
				for ind,(l,m,n) in enumerate(enh):
					enh_1, enh_2 = m, n
					if i>=enh_1-1000 and i<=enh_2+1000:
						t_val[ind] += k
					else:
						background.append(k)
		background = np.array(background)
		if background.shape[0]==0:
			bg.append(0)
		else:
			bg.append(np.mean(background))
		
		t_val_it = t_val.items()
		t_val_it.sort()
		#print g, t_val_it	
		tx = ["%d" % v[1] for v in t_val_it]
		print g, " ".join(tx)

	
	for i in bg:
		print "back", i	

