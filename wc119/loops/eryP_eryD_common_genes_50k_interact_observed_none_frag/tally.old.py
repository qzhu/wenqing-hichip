#!/usr/bin/python
import numpy as np
import sys
import os

def get_direction():
	f = open("/home/qzhu/wc119/loops/mm10.TSS.filtered.sorted.bed")
	t_sign = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		gene = ll[3].split(";")
		sign = ll[-1]
		for g in gene:
			t_sign[g] = sign
	f.close()
	return t_sign

def get_common_start_end(n1, n2):
	f = open(n1)
	all_c = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		all_c.append(c1)
		all_c.append(c2)
	f.close()
	f = open(n2)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		all_c.append(c1)
		all_c.append(c2)
	f.close()
	start = min(all_c)
	end = max(all_c)
	return start, end

def read_file(n, start, end, t_direction):
	f = open(n)
	num_row = (end-start)/1+1
	interact = np.zeros((num_row, num_row), dtype="float32")
	f = open(n)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		val = float(ll[2])
		ind1 = (c1 - start)/1
		ind2 = (c2 - start)/1
		if t_direction=="-":
			ind1 = num_row - 1 - ind1
			ind2 = num_row - 1 - ind2
		interact[ind1, ind2] = val
		interact[ind2, ind1] = val
	f.close()
	return interact

os.chdir("E135")

gene_direction = get_direction()

#200
mat200_135 = np.zeros((200, 200), dtype="float32")
mat200_105 = np.zeros((200, 200), dtype="float32")
num200 = 0

#21
mat201_135 = np.zeros((201, 201), dtype="float32")
mat201_105 = np.zeros((201, 201), dtype="float32")
num201 = 0

for t_file in os.listdir("."):
	print t_file
	gg = t_file.split(".")[1]
	t_direction = gene_direction[gg]
	print t_direction+"a"
	start,end = get_common_start_end(t_file, "../E105/%s" % t_file)
	interact = read_file(t_file, start, end, t_direction)
	interact105 = read_file("../E105/%s" % t_file, start, end, t_direction)

	#interact = np.divide(interact, np.sum(interact)) * 1000
	#interact105 = np.divide(interact105, np.sum(interact105)) * 1000
	#if start!=start105 or end!=end105:
	#	print "BAD"
	print interact.shape[0], interact105.shape[0]

	
	if interact.shape[0]==200:
		mat200_135 = np.add(mat200_135, interact)
		mat200_105 = np.add(mat200_105, interact105)
		num200+=1
	elif interact.shape[0]==201:
		mat201_135 = np.add(mat201_135, interact)
		mat201_105 = np.add(mat201_105, interact105)
		num201+=1
	
#sys.exit(0)
mat200_135 = np.divide(mat200_135, float(num200))
mat200_105 = np.divide(mat200_105, float(num200))

mat201_135 = np.divide(mat201_135, float(num201))
mat201_105 = np.divide(mat201_105, float(num201))

#print mat20_135

print num200, num201
print "MAT201_135"
for i in range(mat201_135.shape[0]):
	for j in range(mat201_135.shape[1]):
		print mat201_135[i,j],
	print ""

print "MAT201_105"
for i in range(mat201_105.shape[0]):
	for j in range(mat201_105.shape[1]):
		print mat201_105[i,j],
	print ""

#print mat20_105	
