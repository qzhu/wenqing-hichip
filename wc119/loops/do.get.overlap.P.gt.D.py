#!/usr/bin/python

import sys
import os
import re

def read_GATA1_map(n):
	f = open(n)
	e135_to_105 = {}
	e105_to_135 = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		e135_to_105.setdefault(ll[0], [])
		xt = ll[1].split(";")
		for x in xt:
			e135_to_105[ll[0]].append(x)
			e105_to_135.setdefault(x, [])
			e105_to_135[x].append(ll[0])
	f.close()
	return e135_to_105, e105_to_135

def read_H3K4me3_map(n):
	f = open(n)
	e135_to_105 = {}
	e105_to_135 = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		e135_to_105.setdefault(ll[3], [])
		xt = ll[14].split(";")
		for x in xt:
			e135_to_105[ll[3]].append(x)
			e105_to_135.setdefault(x, [])
			e105_to_135[x].append(ll[3])
	f.close()
	return e135_to_105, e105_to_135

def read_one_side_k4_gata1_map(n):
	f = open(n)
	peak_to_coord = {}
	peak_to_gene = {}
	peak_to_gata = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		peak = ll[3]
		peak_to_coord[peak] = (ll[0], ll[1], ll[2])
		peak_to_gene[peak] = ll[4]
		peak_to_gata[peak] = ll[5].split(";")
	f.close()
	return peak_to_coord, peak_to_gene, peak_to_gata

if __name__=="__main__":
	gata1_e135_105, gata1_e105_135 = read_GATA1_map("GATA1_DS3_and_PS3_map.bed")
	k4_e135_105, k4_e105_135 = read_H3K4me3_map("/tmp/P_gt_D_H3K4me3_E135_E105_common_genes.sort.bed")
	e135_coord, e135_gene, e135_gata = read_one_side_k4_gata1_map("/tmp/P_gt_D_H3K4me3_E135_E105_common_genes.left.Gata1_DS3.bed")
	e105_coord, e105_gene, e105_gata = read_one_side_k4_gata1_map("/tmp/P_gt_D_H3K4me3_E135_E105_common_genes.right.Gata1_PS3.bed")

	#print e135_gata
	#print k4_e135_105

	for p in e135_gata:
		#print "this_p", p
		#print e135_gata[p]
		if k4_e135_105.has_key(p):
			gata_e135 = e135_gata[p]
			#print gata_e135
			
			gata_e105 = []
			for xp in k4_e135_105[p]:
				if e105_gata.has_key(xp):
					gata_e105.extend(e105_gata[xp])
			#print p, gata_e135, gata_e105
			#print ""

			if len(gata_e135)>0 and len(gata_e105)>0:
				tot_num1 = 0
				for x1 in gata_e135:
					num1 = 0
					if gata1_e135_105.has_key(x1):
						for x2 in gata_e105:
							if x2 in gata1_e135_105[x1]:
								num1+=1
					if num1>0:
						tot_num1+=1
		
				tot_num2 = 0	
				for x2 in gata_e105:
					num2 = 0
					if gata1_e105_135.has_key(x2):
						for x1 in gata_e135:
							if x1 in gata1_e105_135[x2]:
								num2+=1
					if num2>0:
						tot_num2+=1

				#if len(gata_e135) < 3 or len(gata_e105)<3:
				#	continue
				#if float(tot_num1) / len(gata_e135) >= 0.70 and float(tot_num2) / len(gata_e105) >= 0.70:
				if True:
					print e135_coord[p][0], e135_coord[p][1], e135_coord[p][2], p, e135_gene[p], tot_num1, len(gata_e135), tot_num2, len(gata_e105)
