#!/usr/bin/python
import numpy as np
import sys
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy
import scipy.stats
import re
import math
import pandas as pd
import seaborn as sns

def get_direction():
	f = open("/home/qzhu/wc119/loops/mm10.TSS.filtered.sorted.bed")
	t_sign = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		gene = ll[3].split(";")
		sign = ll[-1]
		for g in gene:
			t_sign[g] = sign
	f.close()
	return t_sign

def get_common_start_end(n1, n2):
	f = open(n1)
	all_c = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		all_c.append(c1)
		all_c.append(c2)
	f.close()
	f = open(n2)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		all_c.append(c1)
		all_c.append(c2)
	f.close()
	start = min(all_c)
	end = max(all_c)
	return start, end

def read_file(n, start, end, t_direction):
	f = open(n)
	num_row = (end-start)/1+1
	interact = np.zeros((num_row, num_row), dtype="float32")
	f = open(n)
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, c2 = int(ll[0]), int(ll[1])
		val = float(ll[2])
		ind1 = (c1 - start)/1
		ind2 = (c2 - start)/1
		if t_direction=="-":
			ind1 = num_row - 1 - ind1
			ind2 = num_row - 1 - ind2
		interact[ind1, ind2] = val
		interact[ind2, ind1] = val
	f.close()
	return interact

def read_common(n):
	f = open(n)
	h = f.readline().rstrip("\n").split("\t")
	gene_list = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		gene = ll[1]
		lfc = float(ll[2])
		if lfc<-0.5 and lfc>-1:
			gene_list.append(gene)
	f.close()
	return gene_list

def read_num_enh(n):
	f = open(n)
	glist = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		gene = ll[-1].split()[1]
		num_enh = [int(i) for i in ll[-1].split()[2:]]
		if float(num_enh[0])/num_enh[1]>0.5 and float(num_enh[2])/num_enh[3]>0.5:
			glist.append(gene)
	f.close()
	return glist

def get_upper_triangle(mat_135):
	tri135 = []
	for i in range(101):
		ss = 0
		len_ss = 0
		for j in range(i+1, 101):
			ss+=mat_135[i, j]
			len_ss+=1
		if len_ss>0:
			tri135.append(ss / float(len_ss))
	for i in range(101, 201):
		ss = 0
		len_ss = 0
		for j in range(101, i+1):	
			ss+=mat_135[i, j]
			len_ss+=1
		if len_ss>0:
			tri135.append(ss / float(len_ss))
	tri135 = np.array(tri135)
	return tri135

os.chdir("E135")
glist = read_common("/home/qzhu/wc119/RNA-seq/eryP_eryD_common_genes.txt")
enh_glist = read_num_enh("/tmp/P_gt_D_H3K4me3_E135_E105_common_genes.Gata1.mapped.demo.genes.all.bed")

target_files = []
for t_file in os.listdir("."):
	print t_file
	gg = t_file.split(".")[1]
	if gg in glist and gg in enh_glist:
		target_files.append(t_file)
#print target_files

#sys.exit(0)
gene_direction = get_direction()

#200
mat200_135 = np.zeros((200, 200), dtype="float32")
mat200_105 = np.zeros((200, 200), dtype="float32")
num200 = 0

#21
mat201_135 = np.zeros((201, 201), dtype="float32")
mat201_105 = np.zeros((201, 201), dtype="float32")
num201 = 0

for t_file in target_files:
	print t_file
	gg = t_file.split(".")[1]
	t_direction = gene_direction[gg]
	#print t_direction+"a"
	start,end = get_common_start_end(t_file, "../E105/%s" % t_file)
	interact = read_file(t_file, start, end, t_direction)
	interact105 = read_file("../E105/%s" % t_file, start, end, t_direction)

	#interact = np.divide(interact, np.sum(interact)) * 1000
	#interact105 = np.divide(interact105, np.sum(interact105)) * 1000
	#if start!=start105 or end!=end105:
	#	print "BAD"
	print interact.shape[0], interact105.shape[0]

	
	if interact.shape[0]==200:
		mat200_135 = np.add(mat200_135, interact)
		mat200_105 = np.add(mat200_105, interact105)
		num200+=1
	elif interact.shape[0]==201:
		mat201_135 = np.add(mat201_135, interact)
		mat201_105 = np.add(mat201_105, interact105)
		num201+=1
	
#sys.exit(0)
'''
mat200_135 = np.divide(mat200_135, float(num200))
mat200_105 = np.divide(mat200_105, float(num200))

mat201_135 = np.divide(mat201_135, float(num201))
mat201_105 = np.divide(mat201_105, float(num201))
'''
mat_135 = np.zeros((201, 201), dtype="float32")
mat_105 = np.zeros((201, 201), dtype="float32")
mat_135 = np.copy(mat201_135)
mat_105 = np.copy(mat201_105)

for i in range(200):
	for j in range(200):
		mat_135[i,j] += mat200_135[i,j]
		mat_105[i,j] += mat200_105[i,j]
#print mat20_135

'''
print "MAT201_135"
for i in range(mat201_135.shape[0]):
	for j in range(mat201_135.shape[1]):
		print mat201_135[i,j],
	print ""

print "MAT201_105"
for i in range(mat201_105.shape[0]):
	for j in range(mat201_105.shape[1]):
		print mat201_105[i,j],
	print ""
'''

mat_135 = np.divide(mat_135, float(num201 + num200))
mat_105 = np.divide(mat_105, float(num201 + num200))
print num200, num201

size_factor = 10
colormap="Reds"
f,axn = plt.subplots(1,2,figsize=(size_factor*2,size_factor))
plt.subplots_adjust(hspace=0.01, wspace=0.01)
cm = plt.cm.get_cmap(colormap)

ax = sns.heatmap(pd.DataFrame(mat_135), annot=False, fmt=".2f", ax=axn.flat[0], cmap=colormap)
#plt.savefig("/tmp/mat201.135.png")

tri_135 = get_upper_triangle(mat_135)
tri_105 = get_upper_triangle(mat_105)

for i,j in zip(tri_105, tri_135):
	print i, j

#plt.show()
ax = sns.heatmap(pd.DataFrame(mat_105), annot=False, fmt=".2f", ax=axn.flat[1], cmap=colormap)
plt.savefig("/tmp/mat201.P.gt.D.png")
#plt.show()
#print mat20_105	
a135 = pd.DataFrame(data={"label":["E135" for i in range(201)], "bin":[i for i in range(201)], "freq":[np.mean(mat_135[95:105,i]) for i in range(201)]})
a105 = pd.DataFrame(data={"label":["E105" for i in range(201)], "bin":[i for i in range(201)], "freq":[np.mean(mat_105[95:105,i]) for i in range(201)]})
axn = sns.lmplot(x="bin", y="freq", hue="label", data=a135, fit_reg=False)
axn = sns.lmplot(x="bin", y="freq", hue="label", data=a105, fit_reg=False)
plt.show()
