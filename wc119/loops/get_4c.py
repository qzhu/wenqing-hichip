#!/usr/bin/python

import sys

f = sys.stdin
lines = []
for l in f:
	if l=="": break
	l = l.rstrip("\n")
	ll = l.split("\t")
	lines.append((int(ll[0]), int(ll[1]), float(ll[2])))
f.close()

pos = []
for i,j,k in lines:
	pos.append(i)
	pos.append(j)
pos = list(set(pos))
min_pos = min(pos)
max_pos = max(pos)

reference = sys.argv[1].split(":")
ref_chr = reference[0]
ref_min = int(reference[1])
ref_max = int(reference[2])

frag_size = int(sys.argv[2]) #1, 2, 5

inter = {}
for i,j,k in lines:
	inter.setdefault(i, {})
	inter.setdefault(j, {})
	inter[i][j] = k
	inter[j][i] = k

by_p = {}
p = min_pos
while p<=max_pos:
	#if p>=ref_min and p<=ref_max:
	#	continue
	by_p.setdefault(p, 0)
	if inter.has_key(p):
		q = ref_min
		while q<=ref_max:
			if inter[p].has_key(q):
				by_p[p]+=inter[p][q]
			q+=frag_size
	p+=frag_size

#to_smooth_range = [-2, -1, 0, 1, 2]
#to_smooth_range = [-1, 0, 1]
#to_smooth_range = [0]
to_smooth_range = [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5]
new_by_p = {}
p = min_pos
while p<=max_pos:
	valid = 0
	for a in to_smooth_range:
		n_a = p+a
		if n_a<min_pos or n_a>max_pos:
			continue
		#if n_a>=ref_min and n_a<=ref_max:
		#	continue
		valid+=1
		new_by_p.setdefault(p, 0)
		new_by_p[p]+=by_p.get(n_a, 0)
	if valid>0:
		new_by_p[p] /= float(valid)
	p+=frag_size

#for p in sorted(new_by_p.keys()):
#	print p, new_by_p[p]


t_chr = "chr" + ref_chr
frag_to_bp = {}
f = open("mm10_mboI_cut_site_sort.bed")
for l in f:
	if l=="": break
	l = l.rstrip("\n")
	if l.startswith(t_chr + "\t"):
		ll = l.split("\t")
		frag = int(ll[3].split("_")[2])
		frag_to_bp[frag] = (ll[0], ll[1], ll[2])
f.close()

		
for p in sorted(new_by_p.keys()):
	if frag_size==1:	
		t1, t2, t3 = frag_to_bp[p]
		print t1, t2, t3, new_by_p[p]
	elif frag_size==2:
		t1, t2, t3 = frag_to_bp[p]
		u1, u2, u3 = frag_to_bp[p+1]
		print t1, t2, u3, new_by_p[p]
	elif frag_size==5:
		t1, t2, t3 = frag_to_bp[p]
		u1, u2, u3 = frag_to_bp[p+4]
		print t1, t2, u3, new_by_p[p]	
