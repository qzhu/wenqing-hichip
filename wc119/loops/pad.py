#!/usr/bin/python

import sys

f = open(sys.argv[1])
chrm = ""
cc = {}
for l in f:
	l = l.rstrip("\n")
	ll = l.split("\t")
	chrm = ll[0]
	cc[int(ll[1])] = float(ll[3])
f.close()

cc_min = min(cc.keys())
cc_max = max(cc.keys())

i = cc_min
while True:
	if i>cc_max: break
	if cc.has_key(i):
		sys.stdout.write("%s\t%d\t%d\t%.5f\n" % (chrm, i, i+4999, cc[i]))
	else:
		sys.stdout.write("%s\t%d\t%d\t%.5f\n" % (chrm, i, i+4999, 0))
	i+=5000

