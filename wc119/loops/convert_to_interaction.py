#!/usr/bin/python

import sys

f = open(sys.argv[1])
h = f.readline().rstrip("\n").split("\t")
i = 1
for l in f:
        l = l.rstrip("\n")
        ll = l.split("\t")
        zz = dict(zip(h, ll))
        sys.stdout.write("chr%s\t%s\t%s\tchr%s:%s-%s,%d\t%d\t.\n" % (zz["chr1"], zz["x1"], zz["x2"], zz["chr2"], zz["y1"], zz["y2"], float(zz["observed"]), i))
        sys.stdout.write("chr%s\t%s\t%s\tchr%s:%s-%s,%d\t%d\t.\n" % (zz["chr2"], zz["y1"], zz["y2"], zz["chr1"], zz["x1"], zz["x2"], float(zz["observed"]), i+1))
        i+=2
f.close()

