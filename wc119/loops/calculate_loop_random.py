#!/usr/bin/python

import sys
import numpy as np
import scipy
import scipy.stats

def read(n):
	f = open(n)
	percent = []
	while True:
		l1 = f.readline()
		if l1=="": break
		l1 = l1.rstrip("\n")
		l2 = f.readline().rstrip("\n")
		l3 = f.readline().rstrip("\n")
		l4 = f.readline().rstrip("\n")
		#num1 = l1.split()[-1]
		#num2 = l2.split()[-1]
		num1 = l3.split()[-1]
		num2 = l2.split()[-1]
		num1 = float(num1)
		num2 = float(num2)
		percent.append(float(num2) / float(num1))
	f.close()
	return percent

comp1 = read(sys.argv[1])
comp2 = read(sys.argv[2])

diff = []
for c1, c2 in zip(comp1, comp2):
	#print c1, c2
	diff.append(c2 - c1)

print "c1", np.average(np.array(comp1))
print "c2", np.average(np.array(comp2))
print "diff", np.average(np.array(diff))

nn = np.array(diff)
sc = float(sys.argv[3])
print "score", sc, scipy.stats.percentileofscore(nn, sc)
