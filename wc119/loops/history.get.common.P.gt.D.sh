for i in `cat eryP_eryD_common_genes_eryP_gt_eryD.genes`; do cat mm10.TSS.filtered.sorted.bed |grep "	$i	"; done > /tmp/eryP_eryD_common_genes_eryP_gt_eryD.genes.TSS.bed
sort-bed /tmp/eryP_eryD_common_genes_eryP_gt_eryD.genes.TSS.bed > /tmp/eryP_eryD_common_genes_eryP_gt_eryD.genes.TSS.sort.bed
bedmap --echo --echo-map-id --skip-unmapped --delim "\t" H3K4me3_E105_WC4WC5_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryP_gt_eryD.genes.TSS.sort.bed |~/filter_gene_assignment.py > /tmp/P_gt_D_H3K4me3_E105_common_genes.sort.bed
bedmap --echo --echo-map-id --skip-unmapped --delim "\t" H3K4me3_E135_WC12WC13_merge_sorted_rmdup_peaks.narrowPeak /tmp/eryP_eryD_common_genes_eryP_gt_eryD.genes.TSS.sort.bed |~/filter_gene_assignment.py > /tmp/P_gt_D_H3K4me3_E135_common_genes.sort.bed
bedmap --echo --echo-map --skip-unmapped --delim "\t" /tmp/P_gt_D_H3K4me3_E135_common_genes.sort.bed /tmp/P_gt_D_H3K4me3_E105_common_genes.sort.bed > /tmp/P_gt_D_H3K4me3_E135_E105_common_genes.sort.bed
cat /tmp/P_gt_D_H3K4me3_E135_E105_common_genes.sort.bed|cut -d"	" -f1,2,3,4,11 > /tmp/P_gt_D_H3K4me3_E135_E105_common_genes.left.sort.bed
cat /tmp/P_gt_D_H3K4me3_E135_E105_common_genes.sort.bed|cut -d"	" -f12,13,14,15,22 > /tmp/P_gt_D_H3K4me3_E135_E105_common_genes.right.sort.bed
bedmap --echo --echo-map-id --skip-unmapped --range 50000 --delim "\t" /tmp/P_gt_D_H3K4me3_E135_E105_common_genes.left.sort.bed Gata1_DS3_sort.bed > /tmp/P_gt_D_H3K4me3_E135_E105_common_genes.left.Gata1_DS3.bed
bedmap --echo --echo-map-id --skip-unmapped --range 50000 --delim "\t" /tmp/P_gt_D_H3K4me3_E135_E105_common_genes.right.sort.bed Gata1_PS3_sort.bed > /tmp/P_gt_D_H3K4me3_E135_E105_common_genes.right.Gata1_PS3.bed
#./do.get.overlap.py |sort-bed - > /tmp/H3K4me3_E135_E105_common_genes.Gata1.mapped.demo.genes.bed
