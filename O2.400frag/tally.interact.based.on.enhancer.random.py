#!/usr/bin/python
import sys
import re
import os
import numpy as np
def read_peak_id_to_coord_map(n):
	f = open(n)
	tt = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, s1, s2 = ll[0], int(ll[1]), int(ll[2])
		t_id = ll[3]
		tt[t_id] = (c1, s1, s2)
	f.close()
	return tt

def read_mapping(n, peak_id_to_coord, dist=100000, err=1000):
	f = open(n)
	tt = {}
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		c1, s1, s2 = ll[0], int(ll[1]), int(ll[2])
		g = ll[3]
		enh = ll[4].split(";")
		tt.setdefault(g, {})
		tt[g]["promoter"] = (c1, s1, s2)
		tt[g]["enhancer"] = []
		for e in enh:
			e_chr, e_start, e_end = peak_id_to_coord[e]
			if e_start<=s2+err and e_start>=s1-err:
				continue
			if e_end<=s2+err and e_end>=s1-err:
				continue
			if min(abs(e_start-s1), abs(e_start-s2))<dist or \
			min(abs(e_end-s2), abs(e_end-s1))<dist:
				tt[g]["enhancer"].append((e_chr, e_start, e_end))
	f.close()
	return tt

def read_interact(n):
	f = open(n)
	tt = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		tt.append((int(ll[0]), int(ll[1]), float(ll[2])))
	f.close()
	return tt

def read_interaction(by_g_e10, by_g_e13, tt, mode="E135", genes=None, distance=100000, err=1000):
	ret = []
	bg = []
	interact = None
	#distance = 100000
	bin_size = distance / 20

	#err = 1000
	if genes is None:
		genes = sorted(by_g_e10.keys())

	enh_len = {}
	bg_len = {}

	for g in genes:
		if g not in tt:
			continue
		enh = tt[g]["enhancer"]
		if len(enh)==0:
			continue
		t_len = []
		for ind,(l,m,n) in enumerate(enh):
			enh_1, enh_2 = m,n
			len1 = enh_2 + err - (enh_1 - err)
			t_len.append(len1)
		_, pro_1, pro_2 = tt[g]["promoter"]
		pro_len = pro_2 + err - (pro_1 - err)
		enh_len[g] = np.average(np.array(t_len))
		bg_len[g] = 2*distance - np.sum(np.array(t_len)) - pro_len

	for g in genes:
		if mode=="E135":
			interact = by_g_e13[g]
		else:
			interact = by_g_e10[g]
		if g not in tt:
			continue
		if len(tt[g]["enhancer"])==0:
			continue
		t_val = {}
		t_val_bp = {}
		for ind,(l,m,n) in enumerate(tt[g]["enhancer"]):
			t_val.setdefault(ind, 0)
		background = []
		for i,j,k in interact:
			#print "interaction", i, j, k
			promoter = -1 # 0 means i is promoter, 1 means j is promoter
			pro, enh = tt[g]["promoter"], tt[g]["enhancer"]
			pro_1, pro_2 = pro[1], pro[2] #_1 is start, _2 is end
			if i>=pro_1-err and i<=pro_2+err:
				promoter = 0
			elif j>=pro_1-err and j<=pro_2+err:
				promoter = 1

			if promoter==-1:
				continue
			if promoter==0:
				isEnhancer = False
				for ind,(l,m,n) in enumerate(enh):
					enh_1, enh_2 = m, n
					if j>=enh_1-err and j<=enh_2+err and abs(i-j)<distance:
						t_val[ind] += k
						bin_id = int(abs(i-j) / float(bin_size))
						t_val_bp.setdefault(bin_id, 0)
						t_val_bp[bin_id]+=k
						isEnhancer = True
				if isEnhancer==False and (j<=pro_1-err or j>=pro_2-err) and abs(i-j)<distance:
					background.append(k)
			elif promoter==1:
				isEnhancer = False
				for ind,(l,m,n) in enumerate(enh):
					enh_1, enh_2 = m, n
					if i>=enh_1-err and i<=enh_2+err and abs(i-j)<distance:
						t_val[ind] += k
						bin_id = int(abs(i-j) / float(bin_size))
						t_val_bp.setdefault(bin_id, 0)
						t_val_bp[bin_id]+=k
						isEnhancer = True
				if isEnhancer==False and (i<=pro_1-err or i>=pro_2-err) and abs(i-j)<distance:
					background.append(k)

		den = np.sum(background) / float(bg_len[g]) * enh_len[g]
		#print g, np.sum(background), bg_len[g], enh_len[g], den
		background = np.array(background)
		if background.shape[0]==0:
			bg.append(0)
		else:
			#bg.append(np.sum(background) / bg_len[g] * enh_len[g])
			bg.append(np.average(background))
		
		t_val_it = t_val.items()
		t_val_it.sort()
		#print g, t_val_it	
		tx = ["%d" % v[1] for v in t_val_it]
		#print g, " ".join(tx) #tmp disabled
		t_val_bp_it = t_val_bp.items()
		t_val_bp_it.sort()

		#ret.append(t_val_bp_it)
		ret.append(t_val_it)
	return ret, bg

def read_genes(n):
	genes = []
	f = open(n)
	for l in f:
		l = l.rstrip("\n")
		genes.append(l)
	f.close()
	return genes


if __name__=="__main__":
	peak_map = None
	tt = None
	genes = None
	distance = 50000
	err = 1000

	if sys.argv[1]=="E135":
		peak_map = read_peak_id_to_coord_map("Gata1_DS3_sort.bed")
		#tt = read_mapping("eryD.enhancer.to.promoter.mapping.bed", peak_map, dist=100000)
		#genes = read_genes("eryD.specific.genes.txt")
		tt = read_mapping("random.2D.enhancer.to.promoter.mapping.bed", peak_map, dist=distance, err=err)
		genes = read_genes("random.2.specific.genes.txt")
	else:
		peak_map = read_peak_id_to_coord_map("Gata1_PS3_sort.bed")
		#tt = read_mapping("eryP.enhancer.to.promoter.mapping.bed", peak_map, dist=100000)
		#genes = read_genes("eryP.specific.genes.txt")
		tt = read_mapping("random.2.enhancer.to.promoter.mapping.bed", peak_map, dist=distance, err=err)
		genes = read_genes("random.2.specific.genes.txt")
			
	print "Reading..."	
	by_g_e10 = {}
	by_g_e13 = {}
	if sys.argv[1]=="E135":
		os.chdir("random.2D.genes")
	else:
		os.chdir("random.2.genes")

	num_read = 0
	for fname in os.listdir("."):
		if os.path.isfile(fname):
			xx = fname.split(".")
			gname = xx[1]
			if fname.endswith("E10.5.bp"):
				by_g_e10[gname] = read_interact(fname)
			if fname.endswith("E13.5.bp"):
				by_g_e13[gname] = read_interact(fname)
			num_read+=1
		if num_read%100==0:
			print num_read, "read..."

	genes = [g for g in genes if g in by_g_e10]	
	ret_e10, bg_e10 = read_interaction(by_g_e10, by_g_e13, tt, mode="E105", genes=genes, distance=distance, err=err)
	ret_e13, bg_e13 = read_interaction(by_g_e10, by_g_e13, tt, mode="E135", genes=genes, distance=distance, err=err)

	val_e10, val_e13 = {}, {}
	for ix,iy in zip(ret_e10, ret_e13):
		for it in ix:
			val_e10.setdefault(it[0], [])
			val_e10[it[0]].append(it[1])
		for it in iy:
			val_e13.setdefault(it[0], [])
			val_e13[it[0]].append(it[1])

	print "Averages", np.average(np.array(bg_e10)), np.average(np.array(bg_e13))
	for i in sorted(val_e10.keys()):
		print i, np.average(np.array(val_e10[i])), np.average(np.array(val_e13[i])), len(val_e10[i])
		#print val_e10[i]
		#print val_e13[i]
	#for i in bg:
	#	print "back", i	

