#!/bin/bash
#SBATCH -N 1
#SBATCH -n 4
#SBATCH -t 5-00:00:00
#SBATCH --mem-per-cpu=32gb
#SBATCH -p medium

#SBATCH --mail-user=bernardzhu@gmail.com
#SBATCH --mail-type=end
#SBATCH --job-name=HiCpro_s1_Hichip
#SBATCH --export=ALL
#SBATCH --array=1-5

FASTQFILE=$SLURM_SUBMIT_DIR/inputfiles_Hichip.txt; export FASTQFILE
make --file /home/qz64/usr/HiC-Pro_2.10.0/scripts/Makefile CONFIG_FILE=/n/scratch2/qz64/WC_HiChIP/config-hicpro.txt CONFIG_SYS=/home/qz64/usr/HiC-Pro_2.10.0/config-system.txt all_sub_1a 2>&1
